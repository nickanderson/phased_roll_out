bundle agent cfe_internal_update_policy
{
  meta:
    "description"
      string => "This bundle updates inputs from a policy channel";

  methods:

    # If phase based policy channel is enabled, then we will update
    # the phase data and select a phase and policy channel.

    enable_phased_rollout::

      "Update phase data"
        usebundle => cfe_internal_update_phase_data,
        comment => "We want to use the most recent data when selecting a phase
                    and policy channel";

      "Select Phase and Policy Channel"
        usebundle=> cfe_internal_update_phase_and_channel_selection,
        comment => "We select the appropriate phase and policy channel based on
                    the data provided";

    # Update the policy from the proper source
    any::

      "Update Policy from Selected Channel"
        usebundle => cfe_internal_copy_policy;

}

bundle agent cfe_internal_update_phase_data
{
  meta:
    "description"
      string => "Update phase data (policy channel groups) from policy server
                 so that we use the freshest information available.";

  vars:
    "path[remote_phase_definition]" -> { "Redmine#7443" }
      string => "/var/cfengine/def/phases.json",
      comment => "Path to phase definition on server";

    "path[local_phase_definition]"
      string => "$(sys.workdir)/def/phases.json",
      comment => "Path to local copy of phase definition";

    "path[local_phase_definition_dir]"
      string => dirname("$(path[local_phase_definition])");

  files:
    any::
      "$(path[local_phase_definition_dir])/."
        create => "true",
        comment => "The directory containing the phase definition
                    needs to exist or we get an error from the policy
                    when we attempt to download the file.";

    # The policy server already has the data in place, no need to copy
    # it
    !(policy_server|am_policy_hub)::
      "$(path[local_phase_definition])"
        handle => "cfe_internal_update_phase_data_file",
        copy_from => remote_dcp("$(path[remote_phase_definition])", $(sys.policy_hub)),
        comment => "We want the local phase definition to be up to date with the
		    phase definition on the policy server.";

  reports:
    DEBUG|DEBUG_cfe_internal_update_phase_data::
      "DEBUG $(this.bundle): Remote phase definition path='$(path[remote_phase_definition])'";
      "DEBUG $(this.bundle): Local phase definition path='$(path[local_phase_definition])'";
      "DEBUG $(this.bundle): Local phase definition in sync with remote phase definition"
        depends_on => { "cfe_internal_update_phase_data_file" };
}

bundle agent cfe_internal_update_phase_and_channel_selection
{
  vars:
      # We restrict loading the data file to when the file is present to avoid
      # policy errors when the data file is not present
      # NO need for action immediate, vars aren't promise locked
      # Only readjson if variable does not exists optimization?
      "phase_data"
        data => readjson( "$(cfe_internal_update_phase_data.path[local_phase_definition])", inf),
        ifvarclass => fileexists("$(cfe_internal_update_phase_data.path[local_phase_definition])");

      # This is the a list of the possible phases. The list is numeric in nature
      # as it identifies the index postion of the phase. Here is an example of
      # accessing a phases name: $(phase_data[phases][$(phases)][name])
      "phases"
        slist => getindices("phase_data[phases]");

      # For each potential phase we need to get the list of nodes
      # Nodes can be one of the following:
      #  - hostname regex (fully qualified or short)
      #  - ip address regex (ipv4 or ipv6)
      # Nodes can be one of the following:
      #  - hostname regex (fully qualified or short)
      #  - ip address regex (ipv4 or ipv6)
      #  - hostkey (SHA=31bcb32950d8b91ffdfca85bca71364ec8f67c93246e3617c3a49af58363c4a1)
      #  - Any class defined on the node (Evaluation order has implications on this)
      "phase_$(phases)_matches"
        slist => getvalues("phase_data[phases][$(phases)][nodes]");

      # Find the index of nodegroup that the host is part of.
      "selected_phase"
        int => "$(phases)",
        ifvarclass => or( classmatch(canonify("PK_$(phase_$(phases)_matches)")),
                      classmatch(canonify("$(phase_$(phases)_matches)")),
                      classmatch("$(phase_$(phases)_matches)")),
        classes => u_scoped_classes_generic("bundle", "selected_phase_$(phases)");

      "selected_phase_classes"
        slist => classesmatching("selected_phase_.*");

    # I tried unsuccessfully to use ifelse when setting this variable
    # value, but it was not working out for me, so I fell back to a
    # traditional pattern. Plus potential backwards compatibility issues.
    !successfully_selected_phase::
      "selected_phase_name"
        string => "default",
        meta => { "inventory", "attribute_name=Phase" },
        comment => "We want to inventory the name for use in Mission Portal";

      "selected_phase_policychannel"
        string => "default",
        meta => { "inventory", "attribute_name=Policy Channel" },
        comment => "This is the policy channel that the nodegroup is associated with";


    successfully_selected_phase::
      "selected_phase_name"
        string => "$(phase_data[phases][$(selected_phase)][name])",
        meta => { "inventory", "attribute_name=Phase" },
        comment => "We want to inventory the name for use in Mission Portal";

      # once we have selected a nodegroup we can
      "selected_phase_policychannel"
        string => "$(phase_data[phases][$(selected_phase)][policychannel])",
        meta => { "inventory", "attribute_name=Policy Channel" },
        comment => "This is the policy channel that the nodegroup is associated with";

    any::
      "masterfiles_location"
        string => ifelse( strcmp("default", "$(selected_phase_policychannel)"), "$(update_def.masterfiles_location)", "/var/cfengine/policychannel/$(selected_phase_policychannel)"),
        comment => "If the policy channel is 'default' then the masterfiles location is '$(update_def.masterfiles_location)";

  classes:
      "successfully_selected_phase"
        expression => isvariable("selected_phase");

      "phase_$(selected_phase_name)"
        scope => "namespace",
        meta => { "report" },
        expression => isvariable("selected_phase_name");

      "policy_channel_group_$(selected_phase_name)"
        scope => "namespace",
        meta => { "report" },
        expression => isvariable("selected_phase_name");

      "policy_channel_$(selected_phase_policychannel)"
        scope => "namespace",
        meta => { "report" },
        expression => isvariable("selected_phase_policychannel");

  reports:
#      "$(this.bundle) $(phases)";
#      "$(this.bundle) $(phase_data[phases][0][name])";
#      "$(cfe_internal_update_phase_data.path[local_phase_definition])";
    DEBUG|DEBUG_cfe_internal_update_phase_and_channel_selection::
       "DEBUG $(this.bundle): Defined: '$(selected_phase_classes)'";
#      "DEBUG $(this.bundle): '$(phase_$(phases)_matches)'";
      "DEBUG $(this.bundle):$(const.n)$(const.t)index='$(phases)' $(const.n)$(const.t)name='$(phase_data[phases][$(phases)][name])' $(const.n)$(const.t)MATCH='$(phase_$(phases)_matches)'"
        ifvarclass => or( classmatch(canonify("PK_$(phase_$(phases)_matches)")),
                  classmatch(canonify("$(phase_$(phases)_matches)")),
                  classmatch("$(phase_$(phases)_matches)"));

      "DEBUG $(this.bundle):$(const.n)$(const.t)index='$(phases)' $(const.n)$(const.t)name='$(phase_data[phases][$(phases)][name])' $(const.n)$(const.t)NOMATCH='$(phase_$(phases)_matches)'"
        ifvarclass => not(or( classmatch(canonify("PK_$(phase_$(phases)_matches)")),
                      classmatch(canonify("$(phase_$(phases)_matches)")),
                      classmatch("$(phase_$(phases)_matches)")));


      "DEBUG $(this.bundle): Selected masterfiles policy channel
$(const.t)phase: index='$(selected_phase)'
$(const.t)name='$(selected_phase_name)'
$(const.t)policychannel='$(selected_phase_policychannel)'
$(const.t)Masterfiles should be sourced from:  '$(masterfiles_location)'";
}



body classes u_scoped_classes_generic(scope, x)
# Define x prefixed/suffixed with promise outcome
{
      scope => "$(scope)";
      promise_repaired => { "promise_repaired_$(x)", "$(x)_repaired", "$(x)_ok", "$(x)_reached" };
      repair_failed => { "repair_failed_$(x)", "$(x)_failed", "$(x)_not_ok", "$(x)_error", "$(x)_not_kept", "$(x)_not_repaired", "$(x)_reached" };
      repair_denied => { "repair_denied_$(x)", "$(x)_denied", "$(x)_not_ok", "$(x)_error", "$(x)_not_kept", "$(x)_not_repaired", "$(x)_reached" };
      repair_timeout => { "repair_timeout_$(x)", "$(x)_timeout", "$(x)_not_ok", "$(x)_error", "$(x)_not_kept", "$(x)_not_repaired", "$(x)_reached" };
      promise_kept => { "promise_kept_$(x)", "$(x)_kept", "$(x)_ok", "$(x)_not_repaired", "$(x)_reached" };
}

### FROM STDLIB ###
body copy_from remote_dcp(from,server)
# @brief Download a file from a remote server if it is different from the local copy.
#
# @param from The location of the file on the remote server
# @param server The hostname or IP of the server from which to download
{
      servers     => { "$(server)" };
      source      => "$(from)";
      compare     => "digest";
}
